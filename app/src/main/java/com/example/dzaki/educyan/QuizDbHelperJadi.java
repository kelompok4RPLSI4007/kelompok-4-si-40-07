package com.example.dzaki.educyan;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class QuizDbHelperJadi extends AppCompatActivity {

    EditText soal,optionSatu,optionDua,optionTiga;
    Button tambahSoal,selesai;
    DatabaseReference ref;
    RadioGroup pilihan;
    String level,kelas;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tambahsoal);
        soal = findViewById(R.id.soal);
        optionSatu = findViewById(R.id.op1);
        optionDua = findViewById(R.id.op2);
        optionTiga = findViewById(R.id.op3);
        pilihan = findViewById(R.id.pilihan);
        tambahSoal = findViewById(R.id.tmbhSoal);
        selesai = findViewById(R.id.selesai);
        level = getIntent().getStringExtra("level");
        kelas = getIntent().getStringExtra("kelas");
        ref = FirebaseDatabase.getInstance().getReference().child("materi").child(level).child(kelas).child("quiz");
    }


    public void tambahSoalku(View v){
        String idQuiz = ref.push().getKey();
        ref = ref.child(idQuiz);
        ref.child("soal").setValue(soal.getText().toString());
        ref.child("A").setValue(optionSatu.getText().toString());
        ref.child("B").setValue(optionDua.getText().toString());
        ref.child("C").setValue(optionTiga.getText().toString());


        RadioButton selected = findViewById(pilihan.getCheckedRadioButtonId());
        if (selected==null){
            Toast.makeText(this, "Pilih Jawaban yang benar", Toast.LENGTH_SHORT).show();
            pilihan.requestFocus();
            return;
        }else{
            ref.child("Jawaban").setValue(selected.getText().toString());
        }

        Toast.makeText(this, "Soal ditambahkan", Toast.LENGTH_SHORT).show();
        soal.setText("");
        pilihan.setSelected(false);
        optionSatu.setText("");
        optionDua.setText("");
        optionTiga.setText("");

//        String soalAmbil = soal.getText().toString();
//        String optionSatuAmbil = soal.getText().toString();
//        String optionDuaAmbil = soal.getText().toString();
//        String optionTigaAmbil = soal.getText().toString();
//        int yangBenarAmbil = Integer.parseInt(soal.getText().toString());
//        Question question = new Question(soalAmbil,optionSatuAmbil,optionDuaAmbil,optionTigaAmbil,yangBenarAmbil);
//        QuizDbHelper quizDbHelper = new QuizDbHelper(this);
//        quizDbHelper.addQuestion(question);
    }

    public void done(View view){
        finish();
    }
}
