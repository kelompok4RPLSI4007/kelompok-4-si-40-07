package com.example.dzaki.educyan;

public class Feedback {
    String feedback, author;

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Feedback(String feedback, String author) {
        this.feedback = feedback;
        this.author = author;
    }
}
