package com.example.dzaki.educyan;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


/**
 * A simple {@link Fragment} subclass.
 */
public class ThirdFragment extends Fragment {

    EditText name, password, phone,bio;
    FirebaseAuth mAuth;
    DatabaseReference ref;
    String nameX, phoneX;
    FirebaseUser user;

    public ThirdFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_third, container, false);
        mAuth = FirebaseAuth.getInstance();
        name = view.findViewById(R.id.name);
        phone = view.findViewById(R.id.phone);
        password = view.findViewById(R.id.pwd);
        bio = view.findViewById(R.id.bio);
        user = mAuth.getCurrentUser();
        init();

        if (mAuth.getUid()==null){
            getActivity().startActivity(new Intent(getContext(),MainActivity.class));
            getActivity().finish();
        }
        Button update = view.findViewById(R.id.buttonUpdate);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                update(v);
            }
        });

        Button delete = view.findViewById(R.id.buttonDelete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteAccount(v);
            }
        });

        return view;
    }


    private void theme() {
        SharedPreferences prefs = this.getActivity().getSharedPreferences(this.getActivity().getPackageName(), Context.MODE_PRIVATE);
        if (prefs.getBoolean("nightMode", false)) {
            this.getActivity().setTheme(R.style.dark);
        } else {
            this.getActivity().setTheme(R.style.light);
        }
    }

    private void init(){
        ref = FirebaseDatabase.getInstance().getReference().child("User").child(mAuth.getUid());

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                nameX = dataSnapshot.child("fullname").getValue().toString();
                phoneX = dataSnapshot.child("phone").getValue().toString();

                name.setText(nameX);
                phone.setText(phoneX);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void deleteAccount(final View view){
//        FirebaseUser user = mAuth.getCurrentUser();
        if (user==null){
            startActivity(new Intent(view.getContext(),MainActivity.class));
            this.getActivity().finish();
            Toast.makeText(view.getContext(), "No User", Toast.LENGTH_SHORT).show();
            return;
        }

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("User");
        ref.child(user.getUid()).removeValue();

        user.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    Toast.makeText(view.getContext(), "Success", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(view.getContext(),MainActivity.class));
                    getActivity().finish();
                }
            }
        });
//        AuthCredential credential = EmailAuthProvider
//                .getCredential(mAuth.getCurrentUser().getEmail().toString(),mAuth.getCurrentUser().);
//        user.reauthenticate()
    }

    public void update(final View view) {
        if (!name.getText().toString().equals(nameX)){
            ref.child("fullname").setValue(name.getText().toString());
            Toast.makeText(view.getContext(), "Name updated !", Toast.LENGTH_SHORT).show();
        }
        if (!phone.getText().toString().equals(phoneX)){
            ref.child("phone").setValue(phone.getText().toString());
            Toast.makeText(view.getContext(), "Phone updated !", Toast.LENGTH_SHORT).show();
        }
        if (!password.getText().toString().equals("")){
            user = mAuth.getCurrentUser();
            user.updatePassword(password.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()){
                        Toast.makeText(view.getContext(), "Password updated !", Toast.LENGTH_SHORT).show();
//                        FirebaseUser userX = FirebaseAuth.getInstance().getCurrentUser();

                        AuthCredential credential = EmailAuthProvider
                                .getCredential(mAuth.getCurrentUser().getEmail().toString(), password.getText().toString());
                        user.reauthenticate(credential)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {

                                    }
                                });
                    }
                }
            });
        }
        if (!bio.getText().equals("")) {
            ref.child("bio").setValue(bio.getText().toString());
            Toast.makeText(getContext(), "Bio updated", Toast.LENGTH_SHORT).show();
        }
        init();

    }
}
