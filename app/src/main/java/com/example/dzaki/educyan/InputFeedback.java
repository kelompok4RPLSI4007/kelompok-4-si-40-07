package com.example.dzaki.educyan;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class InputFeedback extends AppCompatActivity {

DatabaseReference ref;
EditText feedback;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.input_feedback);
        feedback = findViewById(R.id.inFeedback);
        ref = FirebaseDatabase.getInstance().getReference().child("feedback");


    }

    public void submitFeedback(View view){
        String push = ref.push().getKey();
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        ref = ref.child(push);
        ref.child("feedback").setValue(feedback.getText().toString());
        ref.child("author").setValue(mAuth.getCurrentUser().getUid());
        Toast.makeText(this, "Feedback sent!", Toast.LENGTH_SHORT).show();
        finish();
    }

    public void cancelFeedback(View view){
        finish();
    }
}





