package com.example.dzaki.educyan;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ActivitySignUp extends AppCompatActivity {

    RadioButton student, teacher;
    EditText email, fullname, phone, password;
    FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        auth = FirebaseAuth.getInstance();

        student = findViewById(R.id.buttonStudent);
        teacher = findViewById(R.id.buttonTeacher);

        email = findViewById(R.id.emailSignUp);
        fullname = findViewById(R.id.fullName);
        phone = findViewById(R.id.edPhone);
        password = findViewById(R.id.pwdSignUp);

    }

    public void signUp(View view) {
        if (test()) {
            auth.createUserWithEmailAndPassword(email.getText().toString(), password.getText().toString())
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("User").child(auth.getUid());
                                ref.child("fullname").setValue(fullname.getText().toString());
                                ref.child("phone").setValue(phone.getText().toString());
                                if (student.isChecked()) {
                                    ref.child("status").setValue("student");
                                } else if (teacher.isChecked()) {
                                    ref.child("status").setValue("teacher");
                                }
                                startActivity(new Intent(ActivitySignUp.this, MainActivity.class));
                                finish();

                            } else {
                                Toast.makeText(ActivitySignUp.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
    }

    public boolean test() {
        if (email.getText().toString().equals("")) {
            Toast.makeText(this, "Isi Email", Toast.LENGTH_SHORT).show();
            email.requestFocus();
            return false;
        }
        if (fullname.getText().toString().equals("")) {
            Toast.makeText(this, "Isi Nama Lengkap", Toast.LENGTH_SHORT).show();
            fullname.requestFocus();
            return false;
        }
        if (phone.getText().toString().equals("")) {
            Toast.makeText(this, "Isi Nomor Telfon", Toast.LENGTH_SHORT).show();
            phone.requestFocus();
            return false;
        }
        if (password.getText().toString().equals("")) {
            Toast.makeText(this, "Isi password", Toast.LENGTH_SHORT).show();
            password.requestFocus();
            return false;
        }
        if (!student.isChecked() && !teacher.isChecked()) {
            Toast.makeText(this, "Pilih Role !", Toast.LENGTH_SHORT).show();
            student.requestFocus();
            return false;
        }
        return true;
    }

    public void login (View view){
       startActivity(new Intent(ActivitySignUp.this,MainActivity.class));
       finish();
    }
}
