package com.example.dzaki.educyan;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ActivityProfile extends AppCompatActivity {

    EditText name, password, phone, bio;
    FirebaseAuth mAuth;
    DatabaseReference ref;
    String nameX, phoneX;
    FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        theme();
        setContentView(R.layout.activity_profile);
        mAuth = FirebaseAuth.getInstance();
        name = findViewById(R.id.name);
        phone = findViewById(R.id.phone);
        password = findViewById(R.id.pwd);
        bio = findViewById(R.id.bio);
        user = mAuth.getCurrentUser();

        init();
//        name = findViewById(R.id.name)
    }

    private void theme() {
        SharedPreferences prefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        if (prefs.getBoolean("nightMode", false)) {
            setTheme(R.style.dark);
        } else {
            setTheme(R.style.light);
        }
    }

    private void init() {
        ref = FirebaseDatabase.getInstance().getReference().child("User").child(mAuth.getUid());

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                nameX = dataSnapshot.child("fullname").getValue().toString();
                phoneX = dataSnapshot.child("phone").getValue().toString();

                name.setText(nameX);
                phone.setText(phoneX);
                if (dataSnapshot.child("bio").exists()) {
                    bio.setText(dataSnapshot.child("bio").getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void deleteAccount(final View view) {
//        FirebaseUser user = mAuth.getCurrentUser();
        if (user == null) {
            startActivity(new Intent(ActivityProfile.this, MainActivity.class));
            finish();
            Toast.makeText(view.getContext(), "No User", Toast.LENGTH_SHORT).show();
            return;
        }

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("User");
        ref.child(user.getUid()).removeValue();

        user.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(view.getContext(), "Success", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(ActivityProfile.this, MainActivity.class));
                    finish();
                }
            }
        });
//        AuthCredential credential = EmailAuthProvider
//                .getCredential(mAuth.getCurrentUser().getEmail().toString(),mAuth.getCurrentUser().);
//        user.reauthenticate()
    }

    public void update(final View view) {
        if (!name.getText().toString().equals(nameX)) {
            ref.child("fullname").setValue(name.getText().toString());
            Toast.makeText(view.getContext(), "Name updated !", Toast.LENGTH_SHORT).show();
        }
        if (!phone.getText().toString().equals(phoneX)) {
            ref.child("phone").setValue(phone.getText().toString());
            Toast.makeText(view.getContext(), "Phone updated !", Toast.LENGTH_SHORT).show();
        }
        if (!password.getText().toString().equals("")) {
            user = mAuth.getCurrentUser();
            user.updatePassword(password.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        Toast.makeText(view.getContext(), "Password updated !", Toast.LENGTH_SHORT).show();
//                        FirebaseUser userX = FirebaseAuth.getInstance().getCurrentUser();

                        AuthCredential credential = EmailAuthProvider
                                .getCredential(mAuth.getCurrentUser().getEmail().toString(), password.getText().toString());
                        user.reauthenticate(credential)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {

                                    }
                                });
                    }
                }
            });

        }

        init();

    }


}



