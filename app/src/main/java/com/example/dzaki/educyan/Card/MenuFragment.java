package com.example.dzaki.educyan.Card;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.dzaki.educyan.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuFragment extends Fragment {
    RecyclerView recyclerView;
    ArrayList<MateriCard> cardArrayList;
    AdapterMateri adapterMateri;
    TextView judul;


    public MenuFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu, container, false);
        // Inflate the layout for this fragment

        judul = view.findViewById(R.id.txJudulMateri);
        judul.setText("Test");


        init();

        return view;
    }

//    public void setJudul(String judul) {
//        this.judul.setText(judul);
//    }

    public void init(){
        cardArrayList.add(new MateriCard("t1","test","test"));
        cardArrayList.add(new MateriCard("t31","3test","te3st"));

    }



}
