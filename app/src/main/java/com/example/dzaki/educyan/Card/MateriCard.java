package com.example.dzaki.educyan.Card;

public class MateriCard {
    String id,foto,judul, deskripsi,author, kelas;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    int idFoto;

    public MateriCard(String kelas,String id, String judul, String deskripsi, String author) {
        this.kelas = kelas;
        this.id = id;
        this.judul = judul;
        this.deskripsi = deskripsi;
        this.author = author;
    }
//
//    public MateriCard(String id, String judul, String deskripsi, int idFoto) {
//        this.id = id;
//        this.judul = judul;
//        this.deskripsi = deskripsi;
//        this.idFoto = idFoto;
//    }

    public int getIdFoto() {
        return idFoto;
    }

    public void setIdFoto(int idFoto) {
        this.idFoto = idFoto;
    }

    public MateriCard(String id, String judul, String deskripsi) {
        this.id = id;
        this.judul = judul;
        this.deskripsi = deskripsi;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }
}
