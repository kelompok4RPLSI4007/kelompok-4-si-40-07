package com.example.dzaki.educyan;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.dzaki.educyan.Card.ViewPagerAdapter;
import com.example.dzaki.educyan.R;
import com.google.firebase.auth.FirebaseAuth;

public class AwalStudent extends AppCompatActivity {
    ViewPager viewPager;
    FirebaseAuth mAuth;
    BottomNavigationView bottomNavigationView;
    FragmentPagerAdapter fragmentPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        theme();
        setContentView(R.layout.activity_awal_student);

        mAuth = FirebaseAuth.getInstance();
        viewPager = findViewById(R.id.viewPager);
        bottomNavigationView = findViewById(R.id.bottomNavigationView);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                bottomNavigationView.getMenu().getItem(i).setChecked(true);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home:
                        viewPager.setCurrentItem(0);
                        break;

                    case R.id.message:
                        viewPager.setCurrentItem(1);
                        break;

                    case R.id.profile:
                        viewPager.setCurrentItem(2);
                        break;
                }
                return true;
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void theme() {
        SharedPreferences prefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        if (prefs.getBoolean("nightMode", false)) {
            setTheme(R.style.dark);
        } else {
            setTheme(R.style.light);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                mAuth.signOut();
                startActivity(new Intent(AwalStudent.this, MainActivity.class));
                finish();
                return true;

            case R.id.edProfile:
                startActivity(new Intent(AwalStudent.this, ActivityProfile.class));
                return true;

            case R.id.dark:
                SharedPreferences prefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);
                boolean darkMode = prefs.getBoolean("nightMode", false);

                SharedPreferences.Editor editor = getSharedPreferences(getPackageName(), MODE_PRIVATE).edit();
                editor.putBoolean("nightMode", !darkMode);
                editor.apply();

                if (darkMode) {
                    setTheme(R.style.dark);

                } else {
                    setTheme(R.style.light);
                }

                recreate();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
