package com.example.dzaki.educyan.Card.Soal;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dzaki.educyan.R;

import java.util.ArrayList;

public class AdapterSoal extends RecyclerView.Adapter<AdapterSoal.ViewHolder>{
    private ArrayList<Soal> daftarSoal;
    private Context mContext;
boolean show =true;

    public AdapterSoal(ArrayList<Soal> daftarSoal, Context mContext) {
        this.daftarSoal = daftarSoal;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public AdapterSoal.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
//        return new AdapterSoal.ViewHolder;
        return new AdapterSoal.ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.soal,viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterSoal.ViewHolder viewHolder, int i) {
        Soal card = daftarSoal.get(i);
        viewHolder.bindTo(card);
    }

    @Override
    public int getItemCount() {
        return daftarSoal.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView soal;
        RadioButton op1,op2,op3;
        Button submit;
        RadioGroup selected;
        String jawaban;

        public ViewHolder(@NonNull final View itemView) {
            super(itemView);

            soal = itemView.findViewById(R.id.txSoal);
            op1 = itemView.findViewById(R.id.rb1);
            op2 = itemView.findViewById(R.id.rb2);
            op3 = itemView.findViewById(R.id.rb3);
            selected = itemView.findViewById(R.id.rgSoal);
            submit = itemView.findViewById(R.id.submit);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    RadioButton x = itemView.findViewById(selected.getCheckedRadioButtonId());
                    if (x!=null){
                        if (jawaban.charAt(0)==x.getText().toString().charAt(0)){
                            Toast.makeText(mContext, "Jawaban Benar", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(mContext, "Jawaban Salah, jawaban yang benar " + jawaban, Toast.LENGTH_SHORT).show();

                        }
                    }else{
                        Toast.makeText(mContext, "Jawab dulu", Toast.LENGTH_SHORT).show();
                    }
                }
            });




            itemView.setOnClickListener(this);
        }


        @SuppressLint("SetTextI18n")
        public void bindTo(Soal card){
            soal.setText(card.getSoal());
            op1.setText("A " +card.getPilihan1());
            op2.setText("B " +card.getPilihan2());
            op3.setText("C " +card.getPilihan3());
            jawaban = card.getJawaban();



        }
        @Override
        public void onClick(View v) {
            if (show){
                selected.setVisibility(View.GONE);
                submit.setVisibility(View.GONE);
                show = false;
            }else{
                selected.setVisibility(View.VISIBLE);
                submit.setVisibility(View.VISIBLE);
                show = true;
            }
//            Toast.makeText(mContext, judul.getText().toString(), Toast.LENGTH_SHORT).show();
        }
    }
}

