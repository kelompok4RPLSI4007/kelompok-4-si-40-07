package com.example.dzaki.educyan;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class AdapterFeedback extends RecyclerView.Adapter<AdapterFeedback.ViewHolder> {
    private ArrayList<Feedback> daftarFeedback;
    private Context mContext;

    public AdapterFeedback(ArrayList<Feedback> daftarFeedback, Context mContext) {
        this.daftarFeedback = daftarFeedback;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public AdapterFeedback.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
//        return new AdapterFeedback.ViewHolder;
        return new AdapterFeedback.ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.feedback, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterFeedback.ViewHolder viewHolder, int i) {
        Feedback card = daftarFeedback.get(i);
        viewHolder.bindTo(card);
    }

    @Override
    public int getItemCount() {
        return daftarFeedback.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView feedback,author;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            feedback = itemView.findViewById(R.id.txFeedback);
            author = itemView.findViewById(R.id.txuser);


            itemView.setOnClickListener(this);
        }


        @SuppressLint("SetTextI18n")
        public void bindTo(Feedback card) {
            feedback.setText("\"" + card.getFeedback() + "\"");

            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("User").child(card.getAuthor());
            ref.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.child("fullname").exists()){
                        author.setText(dataSnapshot.child("fullname").getValue().toString());
                        return;
                    }else{
                        author.setText("anonim");
                        return;
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }

        @Override
        public void onClick(View v) {
//            Toast.makeText(mContext, judul.getText().toString(), Toast.LENGTH_SHORT).show();
        }
    }
}